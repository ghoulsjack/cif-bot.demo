<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%cif_accounts}}".
 *
 * @property int $user_id
 * @property string $username
 * @property string $password
 * @property int $sell_type
 * @property int $units
 * @property string $sec_pwd
 * @property int $active
 * @property int $started_at
 * @property int $stoped_at
 * @property string $last_value
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class CifAccounts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cif_accounts}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // [['user_id', 'username', 'password', 'units', 'sec_pwd'], 'required'],
            [['user_id', 'username'], 'required'],
            [['user_id', 'sell_type', 'units', 'active', 'started_at', 'stoped_at', 'created_at', 'updated_at', 'pid'], 'integer'],
            [['username', 'password', 'sec_pwd', 'last_value'], 'string', 'max' => 255],
            [['user_id', 'username'], 'unique', 'targetAttribute' => ['user_id', 'username']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'sell_type' => Yii::t('app', 'Sell Type'),
            'units' => Yii::t('app', 'Units'),
            'sec_pwd' => Yii::t('app', 'Security Password'),
            'active' => Yii::t('app', 'Active'),
            'started_at' => Yii::t('app', 'Started At'),
            'stoped_at' => Yii::t('app', 'Stoped At'),
            'last_value' => Yii::t('app', 'Last Value'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
}
