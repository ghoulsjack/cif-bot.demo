<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%log_bot}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $username
 * @property int $status
 * @property string $message
 * @property int $created_at
 *
 * @property User $user
 */
class LogBot extends \yii\db\ActiveRecord
{
    const STATUS_INFO = 0;
    const STATUS_WARNING = 1;
    const STATUS_ERROR = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%log_bot}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'username'], 'required'],
            [['user_id', 'status', 'created_at'], 'integer'],
            [['message'], 'string'],
            [['username'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'username' => Yii::t('app', 'Username'),
            'status' => Yii::t('app', 'Status'),
            'message' => Yii::t('app', 'Message'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
