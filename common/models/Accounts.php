<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%accounts}}".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property int $sell_type
 * @property int $units
 * @property string $sec_pwd
 * @property int $active
 * @property string $target_value
 * @property int $created_at
 * @property int $updated_at
 */
class Accounts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%accounts}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password', 'units', 'sec_pwd'], 'required'],
            [['sell_type', 'units', 'active', 'created_at', 'updated_at'], 'integer'],
            [['username', 'password', 'sec_pwd', 'target_value'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'sell_type' => Yii::t('app', 'Sell Type'),
            'units' => Yii::t('app', 'Units'),
            'sec_pwd' => Yii::t('app', 'Sec Pwd'),
            'active' => Yii::t('app', 'Active'),
            'target_value' => Yii::t('app', 'Target Value'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
