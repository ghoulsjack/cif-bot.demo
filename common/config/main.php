<?php
return [
    'aliases' => [
        // '@bower' => '@vendor/bower-asset',
        '@bower' => '@vendor/yidas/yii2-bower-asset/bower',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        // 'session' => [
            // 'class' => 'yii\web\DbSession',
            // 'class' => 'common\components\SessionComponent',
            // 'name' => '_frontendSessionId', // unique for frontend
            // 'sessionTable' => '{{%session}}',
            // 'timeout' => 86400,
        // ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error', 'warning', 'info', 'trace'],
                ],
            ],
        ],
    ],
];
