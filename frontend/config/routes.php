<?php
return [

    // site
    '<action:\w+>/<id:\w+>' => 'site/<action>',
    '<action:\w+>' => 'site/<action>',
    
    // Normal Routes
    '<controller:\w+>/<id:\d+>' => '<controller>/view',
    '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
];