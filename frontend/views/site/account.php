<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = '帳戶設定';
$this->params['breadcrumbs'][] = $this->title;
?>
<div ng-controller="AccountController">
    <div class="section">
        <div class="row">
            <div class="col-xs-12">
                  <div uib-alert ng-repeat="alert in alerts" ng-class="'alert-' + (alert.type || 'warning')" close="closeAlert($index)">
                    {{alert.msg}}
                  </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-widget">
                    <div class="box-header">
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>帳號</th>
                                    <th>密碼</th>
                                    <th>安全碼</th>
                                    <th>交易單位</th>
                                    <th>售賣類型</th>
                                    <th>參考最低價錢</th>
                                    <th>備註</th>
                                    <th>
                                        <a href="" class="small" ng-click="add()">
                                            <i class="fa fa-plus"></i> 新增
                                        </a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="account in accounts">
                                    <td>{{account.id}}</td>
                                    <td>
                                        <i class="fa fa-check-circle text-success" ng-if="account.active" uib-tooltip="啟用中"></i>
                                        <i class="fa fa-minus-circle text-muted" ng-if="!account.active" uib-tooltip="停用中"></i>
                                        {{account.username}}
                                    </td>
                                    <td>{{account.password}}</td>
                                    <td>{{account.sec_pwd}}</td>
                                    <td>{{account.units | currency:'':0}}</td>
                                    <td>
                                        <span ng-if="account.sell_type == 0">原始股</span>
                                        <span ng-if="account.sell_type == 1">配送股</span>
                                    </td>
                                    <td>{{account.target_value}}</td>
                                    <td>{{account.remark}}</td>
                                    <td>
                                        <button class="btn btn-link btn-sm" ng-click="edit(account)">
                                            <i class="fa fa-edit"></i> 編輯
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>