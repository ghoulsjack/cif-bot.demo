<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = '資訊總覽';
?>
<div ng-controller="IndexController">
    <div class="section">
        <div class="row">
            <div class="col-md-6">
                <div class="box box-danger">
                    <div class="box-header">
                        <h3 class="box-title">電子股票價格</h3>
                        <div class="box-tools pull-right">
                            <small class="text-muted">上次更新：{{conf.shares_price.updated_at | date:'HH:mm:ss - MMM d'}}</small>

                            <button type="button" class="btn btn-box-tool" ng-click="loadPrice()" uib-tooltip="更新價格資料">
                                <i class="fa fa-sync"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th rowspan="2" class="text-center">序號</th>
                                    <th rowspan="2" class="text-center">售賣股價</th>
                                    <th colspan="2" class="text-center small">電子股數量</th>
                                    <th rowspan="2" class="text-center">買入股價</th>
                                    <th rowspan="2" class="text-center">電子股數量</th>
                                </tr>
                                <tr>
                                    <th class="text-center">公司</th>
                                    <th class="text-center">市場</th>
                                </tr>
                            </thead>
                        <tbody>
                            <tr ng-repeat="price in conf.shares_price.value">
                                <td>{{price.id}}</td>
                                <td class="text-green">{{price.sell}}</td>
                                <td>{{price.company}}</td>
                                <td>{{price.market}}</td>
                                <td>{{price.buy}}</td>
                                <td>{{price.amount}}</td>
                            </tr>
                        </tbody>                            
                    </table>
                    </div>
                </div>
                <div class="box box-danger">
                    <div class="box-header">
                        <h3 class="box-title">機器人抓取頁面預覽</h3>
                    </div>
                    <div class="box-body">
                        <iframe src="<?=Yii::getAlias('@web/page.html')?>" style="border:0;width:100%;height:400px;"></iframe>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">系統記錄</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" ng-click="loadLogs()" uib-tooltip="更新系統紀錄">
                                <i class="fa fa-sync"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body" uib-accordion>
                            <div class="box box-widget" uib-accordion-group 
                                ng-repeat="log in logs | orderBy:'-created_at'"
                                ng-click="loadContent(log)"
                                heading="{{log.created_at | date:'MM/d HH:mm:ss'}}">
                                <div class="box-body">
                                    <pre style="max-height: 300px;">{{log.content}}</pre>
                                </div>
                                <div class="box-footer">
                                    <small class="text-muted">上次更新：{{log.updated_at | date:'HH:mm:ss - MMM d'}}</small>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
