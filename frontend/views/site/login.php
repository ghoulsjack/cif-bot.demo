<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = '登入會員';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-box">
    <div class="login-logo">
        <small class="text-muted" style="margin-left:-40px;font-size:18px;">crawlbot.ga </small><b><?=Yii::$app->name?></b>
    </div>

    <div class="login-box-body">
        <p class="login-box-msg text-muted">輸入資料登入會員</p>

        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'rememberMe')->checkbox() ?>
            <!-- 
            <div style="color:#999;margin:1em 0">
                <?= Html::a('重設密碼', ['site/request-password-reset']) ?>.
            </div>
 -->
            <div class="form-group">
                <?= Html::submitButton('登入', ['class' => 'btn btn-primary btn-falt btn-block', 'name' => 'login-button']) ?>
            </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>