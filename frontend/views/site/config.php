<?php

/* @var $this yii\web\View */

$this->title = '系統設定';
?>
<div ng-controller="ConfigController">
    <div class="section">
        <div class="row">
            <div class="col-xs-12">
                  <div uib-alert ng-repeat="alert in alerts" ng-class="'alert-' + (alert.type || 'warning')" close="closeAlert($index)">
                    {{alert.msg}}
                  </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-4">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">系統爬蟲登入帳號</h3><br/>
                        <small class="text-muted">作為系統登入查詢資料用，不會進行交易</small>
                    </div>
                    <div class="box-body">
                        <formly-form model="conf.sys_user" fields="sys_user_fields" form="sys_user_form"></formly-form>
                    </div>
                    <div class="box-footer text-right">
                        <button class="btn btn-primary btn-falt" ng-disabled="sys_user_form.$invalid" ng-click="saveSysUser()">
                            <i class="fa fa-save"></i> 儲存
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
