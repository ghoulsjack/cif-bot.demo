<div class="modal-body">
	<p>確定要刪除嗎？</p>
</div>
<div class="modal-footer">
	<button class="btn btn-default btn-flat" ng-click="$ctrl.cancel()"><i class="fa fa-times"></i> 取消</button>
	<button class="btn btn-danger btn-flat" ng-click="$ctrl.del()"><i class="fa fa-check-alt"></i> 確定</button>
</div>