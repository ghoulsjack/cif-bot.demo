<div class="modal-body">
	<p ng-bind="$ctrl.msg"></p>
</div>
<div class="modal-footer">
	<button class="btn btn-default btn-flat" ng-click="$ctrl.cancel()"><i class="fa fa-times"></i> 取消</button>
	<button class="btn btn-primary btn-flat" ng-click="$ctrl.ok()"><i class="fa fa-check-alt"></i> 確定</button>
</div>