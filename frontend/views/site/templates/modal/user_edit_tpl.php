<div class="modal-header">
	<h4 class="modal-title">編輯帳號資訊</h4>
</div>
<div class="modal-body" id="modal-body-{{name}}">
	<div class="row">
		<div class="col-sm-5 col-xs-6">
			<div class="form-group">
				<input type="text" class="form-control input-sm" ng-model="$ctrl.user.last_name" placeholder="姓">
			</div>
		</div>
		<div class="col-sm-5 col-xs-6">
			<div class="form-group">
				<input type="text" class="form-control input-sm" ng-model="$ctrl.user.first_name" placeholder="名">
			</div>
		</div>
		<div class="col-sm-2 col-xs-12">
			<div class="form-group">
				<button class="btn btn-flat btn-success btn-sm btn-block" ng-if="$ctrl.user.status==0" ng-click="$ctrl.toggleActive($ctrl.user)">
					<i class="fa fa-check"></i> 啟用
				</button>
				<button class="btn btn-flat btn-default btn-sm btn-block" ng-if="$ctrl.user.status==10" ng-click="$ctrl.toggleActive($ctrl.user)">
					<i class="fa fa-ban"></i> 停用
				</button>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="form-group">
				<input type="email" class="form-control input-sm" ng-model="$ctrl.user.email" placeholder="Email">
			</div>
		</div>
		<div class="col-xs-12">
			<div class="form-group">
				<input type="text" class="form-control input-sm" ng-model="$ctrl.user.mobile" placeholder="電話">
			</div>
		</div>
		<div class="col-xs-12">
			<div class="form-group">
				<textarea class="form-control" rows="3" placeholder="備註" ng-model="$ctrl.user.remark">
				</textarea>
			</div>
		</div>
	</div>
</div>
<div class="modal-footer">
	<div class="row">
		<div class="col-xs-6 text-left">
			<button class="btn btn-link btn-sm text-maroon" ng-click="$ctrl.delete()">
				<i class="fa fa-trash"></i> 刪除帳號
			</button>
		</div>
		<div class="col-xs-6">
			<button class="btn btn-default btn-flat btn-sm" ng-click="$ctrl.cancel()">
				<i class="fa fa-times"></i> 取消
			</button>
			<button class="btn btn-primary btn-flat btn-sm" ng-click="$ctrl.save()">
				<i class="fa fa-save"></i> 儲存
			</button>
		</div>
	</div>
</div>