<div class="modal-header">
	<h4 class="modal-title">編輯帳號資訊</h4>
</div>
<div class="modal-body" id="modal-body-{{name}}">
	<div class="row">
		<div class="col-xs-12">
	        <formly-form model="$ctrl.account" fields="$ctrl.fields" form="$ctrl.form">
	        </formly-form>
		</div>
	</div>
</div>
<div class="modal-footer">
	<div class="row">
		<div class="col-xs-6 text-left">
			<button class="btn btn-link btn-sm text-maroon" ng-click="$ctrl.delete()" ng-if="$ctrl.action == 'edit'">
				<i class="fa fa-trash"></i> 刪除帳號
			</button>
		</div>
		<div class="col-xs-6">
			<button class="btn btn-default btn-flat btn-sm" ng-click="$ctrl.cancel()">
				<i class="fa fa-times"></i> 取消
			</button>
			<button class="btn btn-success btn-flat btn-sm" ng-disabled="$ctrl.form.$invalid" ng-click="$ctrl.create()" ng-if="$ctrl.action == 'add'">
				<i class="fa fa-plus"></i> 新增
			</button>
			<button class="btn btn-primary btn-flat btn-sm" ng-disabled="$ctrl.form.$invalid" ng-click="$ctrl.save()" ng-if="$ctrl.action == 'edit'">
				<i class="fa fa-save"></i> 儲存
			</button>
		</div>
	</div>
</div>