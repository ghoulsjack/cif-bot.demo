<div class="modal-header">
	<h4 class="modal-title">建立帳號</h4>
</div>
<div class="modal-body" id="modal-body-{{name}}">
	<div class="row">
		<div class="col-xs-12">
	        <formly-form model="$ctrl.user" fields="$ctrl.fields" form="$ctrl.form">
	        </formly-form>
		</div>
	</div>
	<hr/>
	<div class="row">
		<div class="col-xs-6">
			<div class="form-group">
				<input type="text" class="form-control input-sm" ng-model="$ctrl.user.last_name" placeholder="姓">
			</div>
		</div>
		<div class="col-xs-6">
			<div class="form-group">
				<input type="text" class="form-control input-sm" ng-model="$ctrl.user.first_name" placeholder="名">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="form-group">
				<input type="text" class="form-control input-sm" ng-model="$ctrl.user.mobile" placeholder="電話">
			</div>
		</div>
		<div class="col-xs-12">
			<div class="form-group">
				<textarea class="form-control" rows="3" placeholder="備註" ng-model="$ctrl.user.remark">
				</textarea>
			</div>
		</div>
	</div>
</div>
<div class="modal-footer">
	<div class="row">
		<div class="col-xs-12">
			<button class="btn btn-default btn-flat btn-sm" ng-click="$ctrl.cancel()">
				<i class="fa fa-times"></i> 取消
			</button>

			<button class="btn btn-flat btn-sm" ng-class="{false:'btn-primary',true:'btn-default'}[$ctrl.form.$invalid]" ng-disabled="$ctrl.form.$invalid  || $ctrl.submited"  ng-click="$ctrl.create()">
				<i class="fa fa-plus"></i> 建立
			</button>
		</div>
	</div>
</div>