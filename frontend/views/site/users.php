<?php

/* @var $this yii\web\View */

$this->title = '會員管理';
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<style>
.table > tbody > tr > td {
     vertical-align: middle;
}
</style>
<div ng-controller="UserController">
	<div class="row">
		<div class="col-xs-12">
			  <div uib-alert ng-repeat="alert in alerts" ng-class="'alert-' + (alert.type || 'warning')" close="closeAlert($index)">
			  	{{alert.msg}}
			  </div>
		</div>
	</div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-solid">

                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>編號</th>
                                <th>姓名/帳號</th>
                                <th>Email</th>
                                <th>電話</th>
                                <th>帳戶</th>
                                <th>備註</th>
                                <th>機器人</th>
                                <th>
                                	<a href="" class="small" ng-click="create()">
	                                	<i class="fa fa-plus"></i> 建立帳號
	                                </a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="(index,user) in users | filter:searchText | orderBy:'-id'">
                              <td ng-bind="user.id"></td>
                              <td>
                              	<b>{{user.last_name}} {{user.first_name}}</b> <span class="text-muted">{{user.username}}</span> <i class="fas fa-check-circle text-green" uib-tooltip="啟用中" ng-if="user.status==10"></i><i class="fas fa-times-circle text-red" uib-tooltip="停用中" ng-if="user.status==0"></i>
                              </td>
                              <td>{{user.email}}</td>
                              <td>{{user.mobile}}</td>
                              <td>{{user.account}}</td>
                              <td>{{user.remark}}</td>
                              <td>
                                <div class="btn-group" uib-dropdown>
                                    <button ng-if="!user.running" class="btn btn-link btn-xs text-muted small" ng-click="toggleBot(user)">
                                    	休息中 <i class="fa fa-coffee"></i>
                                    </button>

                                    <button ng-if="user.running" class="btn btn-link btn-xs text-blue small" ng-click="toggleBot(user)">
                                    	拼命中 <i class="fa fa-circle-notch fa-spin"></i>
                                    </button>
                                </div>
                              </td>
                              <td>
                              	<button class="btn btn-link btn-xs" ng-click="edit(user)"><i class="fa fa-pencil-alt"></i> 編輯</button>
                              </td>
                            </tr>
                            <tr>
                                <td colspan="4" ng-if="users.length ==0" class="text-center">沒有資料</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>