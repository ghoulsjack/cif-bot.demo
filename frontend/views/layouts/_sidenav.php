<?php
use yii\helpers\Url;
use yii\helpers\Html;
$act = Yii::$app->controller->action->id;
$ctl = Yii::$app->controller->id;
$page = "{$ctl}/{$act}";
?>


<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li style="padding:0 10px;">
                <button class="btn btn-block btn-flat" ng-class="{true:'btn-default',false:'btn-warning'}[running]" style="margin:20px auto;" ng-click="toggleBot()">
                    <span ng-if="!running">啟動機器人爬蟲</span>
                    <span ng-if="running">停用機器人爬蟲</span>
                </button>
            </li>
            <li class="header">主要功能</li>
            <li class="<?=($page=='site/index')?' active':''?>">
                <a href="<?=Yii::$app->homeUrl?>"><i class="fa fa-tachometer-alt"></i> <span>資訊總覽</span></a>
            </li>
                <li class="<?=($page=='site/account')?' active':''?>">
                    <a href="<?=Url::to(['site/account'])?>"><i class="fas fa-user-circle"></i> <span>帳戶設定</span></a>
                </li>
            <?php if(Yii::$app->user->identity->isAdmin):?>
                <li class="header">管理員功能</li>
                <li class="<?=($page=='site/config')?' active':''?>">
                    <a href="<?=Url::to(['site/config'])?>"><i class="fa fa-cog"></i> <span>系統設定</span></a>
                </li>
            <?php endif;?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>