<?php
use yii\helpers\Html;

?>

<header class="main-header">
    <!-- Logo -->
    <a href="<?=Yii::$app->homeUrl?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>CIF</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><small style="opacity:.5;font-size:14px;">crawlbot</small><b>CIF</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button"><i class="fa fa-bars"></i></a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="" class="dropdown-toggle" data-toggle="dropdown">
                      <span class="hidden-xs">哈囉，<?=@Yii::$app->user->identity->first_name?>！</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?=Yii::getAlias('@web/img/deadpool.png')?>" class="img-circle" alt="User Image">
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="row">
                                <div class="col-xs-4 text-center pull-right">
                                  <?=Html::a('登出', ['site/logout'], ['data-method'=>'post'])?>
                                </div>
                            </div>
                            <!-- /.row -->
                        </li>
                    </ul>
                </li>

                <li>
                    <?=Html::a('登出 <i class="fas fa-sign-out-alt"></i>', ['site/logout'], ['data-method'=>'post'])?>
                </li>
            </ul>
        </div>
    </nav>
</header>