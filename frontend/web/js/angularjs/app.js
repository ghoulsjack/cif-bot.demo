var app = angular.module('adminApp', [
	'ngAnimate', 
	'ngTouch', 
	'ui.bootstrap', 
	'formly', 
	'formlyBootstrap',
]);
app
.run(function($rootScope, $location){
	$rootScope.baseUrl = angular.element('meta[name="baseUrl"]').attr('content');
})
.config(['$qProvider', function ($qProvider) {
	$qProvider.errorOnUnhandledRejections(false);
}])
.controller('AccountController', function($rootScope, $scope, $uibModal, AccountService, BotService){
	loadAccounts();

	$scope.alerts = [];

	$scope.addAlert = function(type, msg) {
		// $scope.alerts.push({type:type, msg: msg});
		$scope.alerts = [{type:type, msg: msg}];
	};

	$scope.closeAlert = function(index) {
		$scope.alerts.splice(index, 1);
	};

	$scope.add = function(){
		$uibModal.open({
			animation: true,
			component: 'modalAccountCreateForm',
			resolve: {
				action: function () {
					return 'add';
				},
			},
		}).result.then(function (res) {
			console.log(res);
			if(res=='add'){
				$scope.addAlert('success', '資料新增成功');
				loadAccounts();
			}
		});

	}

	$scope.edit = function (account) {
		$uibModal.open({
			animation: true,
			component: 'modalAccountEditForm',
			resolve: {
				action: function () {
					return 'edit';
				},
				account: function () {
					return account;
				},
			},
		}).result.then(function (res) {
			switch(res){
				case 'save':
					$scope.addAlert('success', account.username+' 資料更新成功');
					break;
				case 'delete':
					AccountService.delAccount({'id':account.id}, function(res){
						if(res.success){
							$scope.addAlert('success', account.username+' 已刪除');
						}else{
							$scope.addAlert('danger', '帳號刪除失敗');
						}
						loadAccounts();
					});
					break;
				case 'cancel':
					break;
			}
		});
	}

	$scope.toggleBot = function(account){
		msg = (account.running) ? '確定要停用嗎？' : '確定要啟用嗎？';

		$uibModal.open({
			animation: true,
			component: 'modalConfirm',
			resolve: {
				msg: function(){
					return msg;
				},
			}
		}).result.then(function (res) {
			if(res == 'ok') {
				if(account.running){
					BotService.deactive(account, function(res){
						// console.log(res);
						loadAccounts();
					});
				}else{
					BotService.active(account, function(res){
						// console.log(res);
						loadAccounts();
					});
				}
    		}
		});
	}

	$scope.toDate = function(timestamp){
		return new Date(timestamp * 1000);
	}

	function loadAccounts() {
		AccountService.getAccounts(function(res){
			$scope.accounts = res.data;
		});
	}
})
.controller('DefaultController', function($rootScope, $scope, $window, $location, $uibModal, DefaultService, BotService){
	loadConfigs();

	$scope.toggleBot = function(){
		msg = ($scope.running) ? '確定要停用嗎？' : '確定要啟用嗎？';

		$uibModal.open({
			animation: true,
			component: 'modalConfirm',
			resolve: {
				msg: function(){
					return msg;
				},
			}
		}).result.then(function (res) {
			if(res == 'ok') {
				if($scope.running){
					BotService.deactive(function(res){
						loadConfigs();
					});
				}else{
					BotService.active(function(res){
						loadConfigs();
					});
				}
    		}
		});
	}

	function loadConfigs(){

		DefaultService.getConfigs(function(res){
			if(res.success) {
				$scope.conf = res.data;
				// console.log($scope.conf);
				if(typeof($scope.conf.processID) !== 'undefined' && $scope.conf.processID != 0 ){
					$scope.running = true;
				}else{
					$scope.running = false;
				}
			}
		});
	}

	$scope.toDate = function(timestamp){
		return new Date(timestamp * 1000);
	}
})
.controller('ConfigController', function($rootScope, $scope, $window, $location, DefaultService){

	DefaultService.getConfigs(function(res){
		if(res.success){
			$scope.conf = res.data;
		}
	});

	$scope.alerts = [];

	$scope.addAlert = function(type, msg) {
		$scope.alerts = [{type:type, msg: msg}];
	};

	$scope.closeAlert = function(index) {
		$scope.alerts.splice(index, 1);
	};

	$scope.saveSysUser = function(){
		DefaultService.setConfigs({'sys_user':$scope.conf.sys_user}, function(res){
			if(res.success){
				$scope.addAlert('success', '資料更新成功');
			}
		});
	}

	$scope.sys_user_fields = [
		{
			key: 'username',
			type: 'input',
			ngModelElAttrs: {
				class: 'form-control input-sm',
			},
			templateOptions: {
				type: 'text',
				placeholder: 'CIF帳戶名稱',
				required: true,
			},
		},
		{
			key: 'password',
			type: 'input',
			ngModelElAttrs: {
				class: 'form-control input-sm',
			},
			templateOptions: {
				type: 'text',
				placeholder: '登入密碼',
				required: true,
			}
		},
	];
})
.controller('IndexController', function($rootScope, $scope, $window, $location, DefaultService){
	loadLogs();
	loadPrice();
	$scope.isCollapse = 0;

	$scope.loadLogs = loadLogs;
	$scope.loadPrice = loadPrice;
	$scope.loadContent = loadContent;

	function loadLogs(){
		DefaultService.getLog(function(res){
			if(res.success){
				$scope.logs = res.data;
				angular.forEach($scope.logs, (e)=>{
					e.created_at = $scope.toDate(e.created_at);
					e.updated_at = $scope.toDate(e.updated_at);
				});
			}
		});
	}

	function loadContent(log){
		DefaultService.getLogContent(log.id, function(res){
			console.log(res);
			if(res.success){
				log.content = res.data.content;
			}
		});
	}

	function loadPrice(){
		DefaultService.getAllConfigs(function(res){
			if(res.success){
				$scope.conf = res.data;
				angular.forEach($scope.conf, (e)=>{
					e.created_at = $scope.toDate(e.created_at);
					e.updated_at = $scope.toDate(e.updated_at);
				});
				// console.log($scope.conf);
			}
		});
	}

	$scope.toDate = function(timestamp){
		return new Date(timestamp * 1000);
	}
})
// .controller('UserController', function($rootScope, $scope, $uibModal, UserService, BotService){
// 	loadUsers();

// 	$scope.alerts = [];

// 	$scope.addAlert = function(type, msg) {
// 		$scope.alerts.push({type:type, msg: msg});
// 	};

// 	$scope.closeAlert = function(index) {
// 		$scope.alerts.splice(index, 1);
// 	};

// 	$scope.create = function () {
// 		var modalInstance = $uibModal.open({
// 			animation: true,
// 			component: 'modalUserCreateForm',
// 		}).result.then(function (res) {
// 			switch(res){
// 				case 'create':
// 					update(user);
// 					break;
// 				case 'cancel':
// 					break;
// 			}
// 		});
// 	}

// 	$scope.edit = function (user) {
// 		var modalInstance = $uibModal.open({
// 			animation: true,
// 			component: 'modalUserEditForm',
// 			resolve: {
// 				user: function () {
// 					return user;
// 				},
// 			},
// 		}).result.then(function (res) {
// 			switch(res){
// 				case 'save':
// 					update(user);
// 					break;
// 				case 'delete':
// 					UserService.delUser(user, function(res){
// 						if(res.success){
// 							$scope.addAlert('success', user.username+' 已刪除');
// 						}else{
// 							$scope.addAlert('danger', '帳號刪除失敗');
// 						}
// 						loadUsers();
// 					});

// 					break;
// 				case 'cancel':
// 					break;
// 			}
// 		});
// 	}

// 	$scope.toggleBot = function(user){
// 		msg = (user.running) ? '確定要停用嗎？' : '確定要啟用嗎？';

// 		$uibModal.open({
// 			animation: true,
// 			component: 'modalConfirm',
// 			resolve: {
// 				msg: function(){
// 					return msg;
// 				},
// 			}
// 		}).result.then(function (res) {
// 			if(res == 'ok') {
// 				if(user.running){
// 					BotService.deactive({'user_id':user.id}, function(res){
// 						console.log(res);
// 						loadUsers();
// 					});
// 				}else{
// 					BotService.active({'user_id':user.id}, function(res){
// 						console.log(res);
// 						loadUsers();
// 					});
// 				}
//     		}
// 		});
// 	}

// 	$scope.toDate = function(timestamp){
// 		return new Date(timestamp * 1000);
// 	}

// 	function loadUsers() {
// 		UserService.getUsers(function(res){
// 			if(res.success){
// 				$scope.users = res.data;
// 				angular.forEach($scope.users, (user)=>{
// 					user.last_logged = new Date(user.last_logged * 1000);
// 					user.created_at = new Date(user.created_at * 1000);
// 				});
// 			}
// 		});
// 	}

// 	function update(user){
// 		UserService.editUser(user, function(res){
// 			if(res.success){
// 				$scope.addAlert('success', '資料更新成功');
// 			}else{
// 				$scope.addAlert('danger', '資料更新失敗');
// 			}
// 		});
// 		loadUsers();
// 	}
// })
.factory('DefaultService', function($http, $rootScope, $timeout){
	var service = {};

	service.getLog = function(callback){
		defaultGet('api/log/get-all', callback);
	}

	service.getLogContent = function(logID, callback){
		defaultPost('api/log/get-content', {'logID':logID}, callback);
	}

	service.getConfigs = function(callback){
		defaultGet('api/bot/get-configs', callback);
	}

	service.getAllConfigs = function(callback){
		defaultGet('api/bot/get-all-configs', callback);
	}

	service.setConfigs = function(form, callback){
		defaultPost('api/bot/set-configs', form, callback);
	}

	return service;

	function defaultPost(url, form, callback){
		var data = {};
		data = angular.extend(form, data);
		$http.post($rootScope.baseUrl + url, data).then(function (response) {
			callback(response.data);
		}, handleError('Error login'));
	}

	function defaultGet(url, callback){
		$http.get($rootScope.baseUrl + url).then(function (response) {
			callback(response.data);
		}, handleError('Error login'));
	}

    function handleError(error) {
        return function () {
            return { success: false, message: error };
        };
    }
})
.factory('AccountService', function($http, $rootScope, $timeout){
	var service = {};

	service.getAccounts = function(callback){
		defaultGet('api/account/get-all', callback);
	}

	service.addAccount = function(form, callback){
		defaultPost('api/account/create-account', form, callback);
	}

	service.editAccount = function(form, callback){
		defaultPost('api/account/update-account', form, callback);
	}

	service.delAccount= function(form, callback){
		defaultPost('api/account/delete-account', form, callback);
	}

	return service;

	function defaultPost(url, form, callback){
		var data = {};
		data = angular.extend(form, data);
		$http.post($rootScope.baseUrl + url, form).then(function (response) {
			callback(response.data);
		}, handleError('Error login'));
	}

	function defaultGet(url, callback){
		$http.get($rootScope.baseUrl + url).then(function (response) {
			callback(response.data);
		}, handleError('Error login'));
	}

    function handleError(error) {
        return function () {
            return { success: false, message: error };
        };
    }
})
// .factory('UserService', function($http, $rootScope, $timeout){
// 	var service = {};

// 	service.getUsers = function(callback){
// 		defaultGet('api/user/get-users', callback);
// 	}

// 	service.currentUser = function(callback){
// 		defaultGet('api/user/current-user', callback);
// 	}

// 	service.resetPassword = function(form, callback){
// 		defaultPost('api/user/reset-password', form, callback);
// 	}

// 	service.createUser = function(form, callback){
// 		defaultPost('api/user/register', form, callback);
// 	}

// 	service.editUser = function(form, callback){
// 		defaultPost('api/user/update-user', form, callback);
// 	}

// 	service.delUser = function(form, callback){
// 		defaultPost('api/user/delete-user', form, callback);
// 	}

// 	return service;

// 	function defaultPost(url, form, callback){
// 		var data = {};
// 		data = angular.extend(form, data);
// 		$http.post($rootScope.baseUrl + url, form).then(function (response) {
// 			callback(response.data);
// 		}, handleError('Error login'));
// 	}

// 	function defaultGet(url, callback){
// 		$http.get($rootScope.baseUrl + url).then(function (response) {
// 			callback(response.data);
// 		}, handleError('Error login'));
// 	}

//     function handleError(error) {
//         return function () {
//             return { success: false, message: error };
//         };
//     }
// })
.factory('BotService', function($http, $rootScope, $timeout){
	var service = {};

	service.savePID = function(form, callback){
		defaultPost('api/bot/save-pid', form, callback);
	}

	service.active = function(callback){
		defaultGet('api/bot/active', callback);
	}

	service.deactive = function(callback){
		defaultGet('api/bot/deactive', callback);
	}

	return service;

	function defaultPost(url, form, callback){
		var data = {};
		data = angular.extend(form, data);
		$http.post($rootScope.baseUrl + url, form).then(function (response) {
			callback(response.data);
		}, handleError('Error login'));
	}

	function defaultGet(url, callback){
		$http.get($rootScope.baseUrl + url).then(function (response) {
			callback(response.data);
		}, handleError('Error login'));
	}

    function handleError(error) {
        return function () {
            return { success: false, message: error };
        };
    }
})
.component('modalDelConfirm', {
  templateUrl: 'template?content=modal/del_tpl',
  bindings: {
	resolve: '<',
	close: '&',
	dismiss: '&'
  },
  controller: function () {
	var $ctrl = this;
    $ctrl.$onInit = function () {
    	$ctrl.cate = $ctrl.resolve.cate;
    };

    $ctrl.del = function () {
    	$ctrl.close({$value: true});
    };

    $ctrl.cancel = function () {
    	$ctrl.close({$value: false});
    };
  }
})
.component('modalConfirm', {
  templateUrl: 'template?content=modal/confirm_tpl',
  bindings: {
	resolve: '<',
	close: '&',
	dismiss: '&'
  },
  controller: function () {
	var $ctrl = this;
    $ctrl.$onInit = function () {
    	$ctrl.msg = $ctrl.resolve.msg;
    };

    $ctrl.cancel = function () {
    	$ctrl.close({$value: 'cancel'});
    };

    $ctrl.ok = function () {
    	$ctrl.close({$value: 'ok'});
    };
  }
})
// .component('modalUserEditForm', {
//   templateUrl: 'template?content=modal/user_edit_tpl',
//   bindings: {
// 	resolve: '<',
// 	close: '&',
// 	dismiss: '&'
//   },
//   controller: function (UserService, $uibModal) {
// 	var $ctrl = this;

//     $ctrl.$onInit = function () {
//     	$ctrl.user = $ctrl.resolve.user;
//     };

//     $ctrl.delete = function() {
//     	$uibModal.open({
// 			animation: true,
// 			component: 'modalDelConfirm',
// 		}).result.then(function (res) {
// 			if(res){
//     			$ctrl.close({$value: 'delete'});
// 			}
// 		});
//     }

//     $ctrl.toggleActive = function(user){
//     	switch(user.status){
//     		case 0:
//     			msg = '確定要啟用 '+user.username+' 嗎？';
//     			break;
//     		case 10:
//     			msg = '確定要停用 '+user.username+' 嗎？';
//     			break;
//     	}
// 		$uibModal.open({
// 			animation: true,
// 			component: 'modalConfirm',
// 			resolve: {
// 				msg: function(){
// 					return msg;
// 				},
// 			}
// 		}).result.then(function (res) {
// 			if(res == 'ok') {
//     			// $ctrl.close({$value: 'toggleActive'});
// 				if(user.status == 0){
// 					user.status = 10;
// 				}else if(user.status == 10){
// 					user.status = 0;
// 				}
//     		}
// 		});
//     }

//     $ctrl.cancel = function () {
//     	$ctrl.close({$value: 'cancel'});
//     };

//     $ctrl.save = function () {
//     	$ctrl.close({$value: 'save'});
//     };
//   }
// })
.component('modalAccountEditForm', {
  templateUrl: 'template?content=modal/account_edit_tpl',
  bindings: {
	resolve: '<',
	close: '&',
	dismiss: '&'
  },
  controller: function ($uibModal, AccountService) {
	var $ctrl = this;

    $ctrl.$onInit = function () {
    	$ctrl.account = $ctrl.resolve.account;
    	$ctrl.action = $ctrl.resolve.action;

		$ctrl.fields = [
			{
				key: 'username',
				type: 'input',
				ngModelElAttrs: {
					class: 'form-control input-sm',
				},
				templateOptions: {
					type: 'text',
					placeholder: 'CIF帳戶名稱',
					required: true,
				},
			},
			{
				key: 'password',
				type: 'input',
				ngModelElAttrs: {
					class: 'form-control input-sm',
				},
				templateOptions: {
					type: 'text',
					placeholder: '登入密碼',
					required: true,
				}
			},
			{
				key: 'sec_pwd',
				type: 'input',
				ngModelElAttrs: {
					class: 'form-control input-sm',
				},
				templateOptions: {
					type: 'text',
					placeholder: '安全碼',
					required: true,
				}
			},
			{
				key: 'units',
				type: 'input',
				ngModelElAttrs: {
					class: 'form-control input-sm',
				},
				templateOptions: {
					type: 'number',
					placeholder: '交易單位',
					required: true,
				}
			},
			{
				key: "sell_type",
				type: "select",
				ngModelElAttrs: {
					class: 'form-control input-sm',
					style: 'border-radius:0;',
				},
				templateOptions: {
					label: "售賣類型",
					options: [
						{
							"name": "原始股",
							"value": 0
						},
						{
							"name": "配送股",
							"value": 1
						}
					]
				}
			},
			{
				key: 'target_value',
				type: 'input',
				ngModelElAttrs: {
					class: 'form-control input-sm',
				},
				templateOptions: {
					type: 'text',
					placeholder: '參考最低價錢',
				},
			},
			{
				key: 'active',
				type: 'checkbox',
				defaultValue: true,
				templateOptions: {
					label: '啟用',
				}
			},
			{
				key: 'remark',
				type: 'textarea',
				ngModelElAttrs: {
					class: 'form-control input-sm',
				},
				templateOptions: {
					type: 'textarea',
					placeholder: '備註',
					rows: 5,
					grow: false
				}
			},
		];
    };

    $ctrl.delete = function() {
    	$uibModal.open({
			animation: true,
			component: 'modalDelConfirm',
		}).result.then(function (res) {
			if(res){
    			$ctrl.close({$value: 'delete'});
			}
		});
    }

    $ctrl.cancel = function () {
    	$ctrl.close({$value: 'cancel'});
    };

    $ctrl.save = function () {
    	$ctrl.account.active = ($ctrl.account.active) ? 1 : 0;
		AccountService.editAccount($ctrl.account, function(res){
			if(res.success){
    			$ctrl.close({$value: 'save'});
			}else{
				console.log(res);
			}
		});
    };
  }
})
.component('modalAccountCreateForm', {
  templateUrl: 'template?content=modal/account_edit_tpl',
  bindings: {
	resolve: '<',
	close: '&',
	dismiss: '&'
  },
  controller: function (AccountService) {
	var $ctrl = this;

	$ctrl.fields = [
		{
			key: 'username',
			type: 'input',
			ngModelElAttrs: {
				class: 'form-control input-sm',
			},
			templateOptions: {
				type: 'text',
				placeholder: 'CIF帳戶名稱',
				required: true,
			},
		},
		{
			key: 'password',
			type: 'input',
			ngModelElAttrs: {
				class: 'form-control input-sm',
			},
			templateOptions: {
				type: 'text',
				placeholder: '登入密碼',
				required: true,
			}
		},
		{
			key: 'sec_pwd',
			type: 'input',
			ngModelElAttrs: {
				class: 'form-control input-sm',
			},
			templateOptions: {
				type: 'text',
				placeholder: '安全碼',
				required: true,
			}
		},
		{
			key: 'units',
			type: 'input',
			ngModelElAttrs: {
				class: 'form-control input-sm',
			},
			templateOptions: {
				type: 'number',
				placeholder: '交易單位',
				required: true,
			}
		},
		{
			key: "sell_type",
			type: "select",
			ngModelElAttrs: {
				class: 'form-control input-sm',
				style: 'border-radius:0;',
			},
			templateOptions: {
				label: "售賣類型",
				required: true,
				options: [
					{
						"name": "原始股",
						"value": 0
					},
					{
						"name": "配送股",
						"value": 1
					}
				]
			}
		},
		{
			key: 'target_value',
			type: 'input',
			ngModelElAttrs: {
				class: 'form-control input-sm',
			},
			templateOptions: {
				type: 'text',
				placeholder: '參考最低價錢',
			},
		},
		{
			key: 'remark',
			type: 'textarea',
			ngModelElAttrs: {
				class: 'form-control input-sm',
			},
			templateOptions: {
				type: 'textarea',
				placeholder: '備註',
				rows: 5,
				grow: false
			}
		},
	];

    $ctrl.$onInit = function () {
    	$ctrl.account = {};
    	$ctrl.action = $ctrl.resolve.action;
    };

    $ctrl.cancel = function () {
    	$ctrl.close({$value: false});
    };

    $ctrl.create = function () {
    	AccountService.addAccount($ctrl.account, function(res){
    		if(res.success){
    			$ctrl.close({$value: 'create'});
    		}else{
    			console.log(res);
    		}
    	});
    	$ctrl.close({$value: 'add'});
    };
  }
})
// .component('modalUserCreateForm', {
//   templateUrl: 'template?content=modal/user_create_tpl',
//   bindings: {
// 	resolve: '<',
// 	close: '&',
// 	dismiss: '&'
//   },
//   controller: function (UserService) {
// 	var $ctrl = this;

// 	$ctrl.fields = [
// 		{
// 			key: 'username',
// 			type: 'input',
// 			ngModelElAttrs: {
// 				class: 'form-control input-sm',
// 			},
// 			templateOptions: {
// 				type: 'text',
// 				label: '使用者帳號',
// 				placeholder: '請輸入使用者帳號',
// 				required: true,
// 				minlength: 6,
// 				onKeydown: function(value, options) {
// 					options.validation.show = false;
// 				},
// 				onBlur: function(value, options) {
// 					options.validation.show = null;
// 				}
// 			},
// 	        modelOptions: {
// 	          updateOn: 'blur'
// 	        },
// 		},
// 		{
// 			key: 'password',
// 			type: 'input',
// 			ngModelElAttrs: {
// 				class: 'form-control input-sm',
// 			},
// 			templateOptions: {
// 				type: 'password',
// 				label: '密碼',
// 				placeholder: '請輸入長度至少 6 位密碼',
// 				required: true,
// 				minlength: 6
// 			}
// 		},
// 		{
// 			key: 'checkPassword',
// 			type: 'input',
// 			ngModelElAttrs: {
// 				class: 'form-control input-sm',
// 			},
// 			optionsTypes: ['matchField'],
// 			model: $ctrl.confirmationModel,
// 			templateOptions: {
// 				type: 'password',
// 				label: '密碼確認',
// 				placeholder: '請再次輸入密碼',
// 				required: true,
// 			},
// 			data: {
// 				fieldToMatch: 'password',
// 				modelToMatch: $ctrl.model
// 			}
// 		},
// 		{
// 			key: 'email',
// 			type: 'input',
// 			ngModelElAttrs: {
// 				class: 'form-control input-sm',
// 			},
// 			templateOptions: {
// 				type: 'email',
// 				placeholder: '請輸入電子信箱',
// 				label: '電子信箱email',
// 				required: true,
// 				maxlength: 255
// 			}
// 		},
// 		{
// 			key: 'account',
// 			type: 'input',
// 			ngModelElAttrs: {
// 				class: 'form-control input-sm',
// 			},
// 			templateOptions: {
// 				type: 'text',
// 				label: 'CIF帳戶名稱',
// 				placeholder: '請輸入帳戶名稱',
// 				required: true,
// 			},
// 		},
// 	];

//     $ctrl.$onInit = function () {
//     	$ctrl.user = {};
//     };

//     $ctrl.cancel = function () {
//     	$ctrl.close({$value: false});
//     };

//     $ctrl.create = function () {
//     	UserService.createUser($ctrl.user, function(res){
//     		if(res.success){
//     			$ctrl.close({$value: 'create'});
//     		}
//     	});
//     };
//   }
// })
;