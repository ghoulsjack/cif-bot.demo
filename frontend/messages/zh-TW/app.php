<?php
return [
	'Username' => '帳戶名稱',
	'Password' => '登入密碼',
	'Sell Type' => '售賣類型',
	'Units' => '交易單位',
	'Security Password' => '安全密碼',
	'Active' => '啟動',
	'Started At' => '啟動時間',
	'Stoped At' => '停止時間',
	'Last Value' => '上次操作數值',
	'Created At' => '建立時間',
	'Updated At' => '更新時間',
	'Update Success.' => '更新成功',
	'Remember Me' => '記住登入狀態',
];