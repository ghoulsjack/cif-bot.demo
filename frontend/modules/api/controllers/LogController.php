<?php

namespace frontend\modules\api\controllers;

use Yii;
use yii\web\Controller;
use common\models\BotLog;
use common\models\Accounts;

use yii\helpers\Url;

class LogController extends Controller
{
	public function actionGetAll()
	{
		$res = [];
		foreach(BotLog::find()->all() as $log){
			$res[] = [
				'id' => $log->id,
				// 'content' => $log->content,
				'created_at' => $log->created_at,
				'updated_at' => $log->updated_at
			];
		}
		return ['success'=>true, 'data'=>$res];
	}

	public function actionGetContent()
	{
		$post = Yii::$app->request->post();

		if( $log = BotLog::findOne(@$post['logID']) ){
			return ['success'=>true, 'data'=>[
				'id' => $log->id,
				'content' => $log->content,
				'created_at' => $log->created_at,
				'updated_at' => $log->updated_at
			]];
		}

		return ['success'=>false, 'message'=>'System Error'];

	}

	public function actionRunning()
	{
		$post = Yii::$app->request->post();

		if(@$post['API_TOKEN'] !== Yii::$app->params['API_TOKEN'])
			return ['success'=>false, 'message'=>'Permission Denied.'];

		if( $log = BotLog::findOne(@$post['logID']) ){
			$log->content = date('M d H:i:s', strtotime('+8 hour')) . ' | '. $post['message'] . "\r\n" . $log->content;
			$log->updated_at = time();

			if($log->save()){
				return ['success'=>true, 'logID'=>$log->id];
			}
		}

		return ['success'=>false, 'message'=>'System Error'];

	}
}
