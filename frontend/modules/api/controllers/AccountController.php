<?php

namespace frontend\modules\api\controllers;

use Yii;
use yii\web\Controller;
use common\models\User;
use common\models\Accounts;

use yii\helpers\Url;

class AccountController extends Controller
{
    public function actionGetAll()
    {
        $res = [];
        foreach(Accounts::find()->all() as $account){
            $res[] = [
                'id' => $account->id,
                'username' => $account->username,
                'password' => $account->password,
                'sec_pwd' => $account->sec_pwd,
                'units' => $account->units,
                'sell_type' => $account->sell_type,
                'active' => boolval($account->active),
                'target_value' => $account->target_value,
                'remark' => $account->remark,
                'created_at' => $account->created_at,
                'updated_at' => $account->updated_at,
            ];
        }
        return ['success'=>true, 'data'=>$res];
    }

    public function actionCreateAccount()
    {

        $model = new Accounts([
            'active' => 1,
            'created_at' => time(),
            'updated_at' => time()
        ]);

        foreach(Yii::$app->request->post() as $key=>$value){
            $model->$key = $value;
        }
        if($model->save()) return ['success'=>true, 'message'=>'data updated.'];

        return ['success'=>false, 'message'=>'data update failed.'];
    }

    public function actionUpdateAccount()
    {
        $post = Yii::$app->request->post();

        if( $model = Accounts::findOne($post['id']) ){
            $attrs = ['username', 'password', 'sec_pwd', 'units', 'sell_type', 'remark', 'active', 'target_value'];
            foreach ($attrs as $attr) {
                if(isset($post[$attr])){
                    $model->$attr = $post[$attr];
                }
            }
            $model->updated_at = time();
            if($model->save()) return ['success'=>true, 'message'=>'data updated.'];
        }

        return ['success'=>false, 'message'=>'data update failed.'];
    }

    public function actionDeleteAccount()
    {
        $post = Yii::$app->request->post();

        if( $model = Accounts::findOne($post['id']) ){
            if($model->delete()) return ['success'=>true, 'message'=>'data deleted.'];
        }

        return ['success'=>false, 'message'=>'data deleting failed.'];
    }

    private function getErr($model)
    {
        $message = array_values($model->getFirstErrors());
        return (count($message)>0) ? $message[0] : '';
    }
}
