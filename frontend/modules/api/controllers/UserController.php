<?php

namespace frontend\modules\api\controllers;

use Yii;
use yii\web\Controller;
use common\models\User;
use common\models\UserToken;
use common\models\LogBot;

use frontend\models\SignupForm;

use yii\helpers\Url;

class UserController extends Controller
{
	/**
	* @param url
	*/
	public function actionInfo()
	{
		if(!Yii::$app->user->isGuest){
			$user = Yii::$app->user->identity;
			$data = [
				'id' => $user->id,
				'username' => $user->username,
				'first_name' => $user->first_name,
				'last_name' => $user->last_name,
			];
			return ['success'=>true, 'data'=>$data];
		}
		return ['success' => false, 'message'=>'Not Logged.'];
	}

	public function actionLogs()
	{
		if(!Yii::$app->user->isGuest){
			$user = Yii::$app->user->identity;
			$res = [];
			foreach ($user->botLog as $key => $log) {
				if($key < 500){
					$res[] = [
			            'id' => $log->id,
			            'username' => $log->username,
			            'status' => $log->status,
			            'message' => $log->message,
			            'created_at' => $log->created_at,
					];
				}
			}
			return ['success'=>true, 'data'=>$res];
		}
		return ['success' => false, 'message'=>'Not Logged.'];
	}
    
    public function actionGetUsers()
    {
        $res = [];
        // foreach(User::findAll(['not', ['role'=>'admin']]) as $user){
        foreach(User::find()->all() as $user){
            $res[] = [
                'id' => $user->id,
                'username' => $user->username,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'mobile' => $user->mobile,
                'status' => $user->status,
                'remark' => $user->remark,
                'created_at' => $user->created_at,
            ];
        }
        return ['success'=>true, 'data'=>$res];
    }
    public function actionCurrentUser()
    {
        if(!Yii::$app->user->isGuest){
            $user = Yii::$app->user;
            $qta = [];
            foreach($user->identity->quota as $q){
                $qta[] = [
                    'cate_id' => $q->cate->id,
                    'cate_title' => $q->cate->title,
                    'type' => $q->type,
                    'value' => $q->value,
                ];
            }
            $data = [
                'id' => $user->id,
                'username' => $user->identity->username,
                'email' => $user->identity->email,
                'quota' => $qta,
            ];
            return ['success'=>true, 'data'=>$data];
        }
        return ['success'=>false, 'message'=>'user not logged.'];
    }

    public function actionRegister()
    {
        $model = new SignupForm();

        $post = Yii::$app->request->post();
        $model->attributes = $post;

        if( $user = $model->signup() ) {
            return [
                'success'=>true,
                'username' => $user->username,
                'email' => $user->email,
                'auth_key' => $user->auth_key,
            ];
        }
        
        $message = array_values($model->getFirstErrors());
        $message = (count($message)>0) ? $message[0] : '';
        return ['success'=>false,'message'=>$message];
    }

    public function actionUpdateUser()
    {
        $post = Yii::$app->request->post();

        if($model = User::findOne($post['id'])){
        	$attrs = ['status', 'first_name', 'last_name', 'mobile', 'remark', 'email'];
        	foreach ($attrs as $attr) {
        		if(isset($post[$attr])){
        			$model->$attr = $post[$attr];
        		}
        	}
            $model->updated_at = time();
            if($model->save()) return ['success'=>true, 'message'=>'data updated.'];
        }

        return ['success'=>false, 'message'=>'data update failed.'];
    }

    public function actionDeleteUser()
    {
        $post = Yii::$app->request->post();

        if($model = User::findOne($post['id'])){
            if($model->delete()) return ['success'=>true, 'message'=>'data deleted.'];
        }

        return ['success'=>false, 'message'=>'data deleting failed.'];
    }

    private function getErr($model)
    {
        $message = array_values($model->getFirstErrors());
        return (count($message)>0) ? $message[0] : '';
    }
}
