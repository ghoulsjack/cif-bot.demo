<?php
namespace frontend\modules\api\controllers;

use Yii;
use yii\web\Controller;
use common\models\Accounts;
use common\models\User;
use common\models\BotLog;
use common\models\SystemConfig;

use yii\helpers\Url;

class BotController extends Controller
{
	public function actionTest()
	{
	    $file = \Yii::getAlias('@python/crawler.py');
	    $command = "/usr/bin/python3 {$file} 66635869 myles1101 myles1101 " . Url::base(Yii::$app->params['urlType']);

		$res = shell_exec("{$command} 2>&1");

		print_r($res);
		exit();
	}

	// 紀錄/更新抓取的電子股票價格資料
	public function actionUpdateSharesPrice()
	{
		$post = Yii::$app->request->post();

		if($post['API_TOKEN'] !== Yii::$app->params['API_TOKEN'])
			return ['success'=>false, 'message'=>'Permission Denied.'];

		if($config = SystemConfig::findOne(['key'=>'shares_price'])){
			$config->setAttributes([
				'value' => $post['data'],
				'updated_at' => time(),
			]);
		}else{
			$config = new SystemConfig([
				'key' => 'shares_price',
				'type' => 'json',
				'value' => $post['data'],
				'updated_at' => time(),
			]);
		}

		if($config->save()){
			return ['success'=>true, 'message'=>'Data Updated'];
		}

		return ['success'=>false, 'message'=>'System Error'];

	}

	public function actionActive()
	{
		$conf = $this->getConfigs();

		$log = new BotLog([
			'created_at' => time(),
			'updated_at' => time()
		]);

		$log->save();

		$sys_user = $conf['sys_user'];

        $file = \Yii::getAlias('@python/crawler.py');

        $command = "/usr/bin/python3 {$file} {$log->id} " . Url::base(Yii::$app->params['urlType']);

		$pid = shell_exec("/usr/bin/nohup {$command} > /dev/null 2> /dev/null & echo $!");

		$this->setConfig('processID', 'integer', $pid);

		return ['success'=>true, 'pid'=>intval($pid)];
	}

	public function actionSavePid()
	{

		$post = Yii::$app->request->post();

		if($this->setConfig('processID', 'integer', $post['pid'])){
			return ['success'=>true, 'message'=>'Data Updated.'];
		}

		return ['success'=>false, 'message'=>'System Error'];

	}

	public function actionDeactive()
	{
		$conf = $this->getConfigs();

		exec("kill {$conf['processID']}");

		$this->setConfig('processID', 'integer', null);

		return ['success'=>true];
	}

	public function actionUpdateLastValue()
	{
		$post = Yii::$app->request->post();

		if($post['API_TOKEN'] !== Yii::$app->params['API_TOKEN'])
			return ['success'=>false, 'message'=>'Permission Denied.'];

		if($account = Accounts::findByUsername(@$post['username'])){
			$account->last_value = $post['last_value'];
			$account->updated_at = time();
			$account->save();

			return ['success'=>true, 'message'=>'Data Updated.'];
		}

		return ['success'=>false, 'message'=>'Data Error'];
	}

	public function actionSettings()
	{
		$post = Yii::$app->request->post();

		if($post['API_TOKEN'] !== Yii::$app->params['API_TOKEN'])
			return ['success'=>false, 'message'=>'Permission Denied.'];

		$activeAccounts = [];
		foreach(Accounts::findAll(['active'=>true]) as $account){
			$activeAccounts[] = [
				'username' => $account->username,
				'password' => $account->password,
				'sec_pwd' => $account->sec_pwd,
				'units' => $account->target_value,
				'sell_type' => $account->target_value,
				'target_value' => $account->target_value,
			];
		}

		$res = $this->getConfigs();
		$res['accounts'] = $activeAccounts;

		return ['success'=>true, 'data'=>$res];
	}

	public function actionStop()
	{
		$post = Yii::$app->request->post();

		if($post['API_TOKEN'] !== Yii::$app->params['API_TOKEN'])
			return ['success'=>false, 'message'=>'Permission Denied.'];

		$this->setConfig('processID', 'integer', null);

		return ['success'=>true, 'data'=>$res];
	}

	public function actionGetConfigs()
	{
		$activeAccounts = [];
		foreach(Accounts::findAll(['active'=>true]) as $account){
			$activeAccounts[] = [
				'username' => $account->username,
				'password' => $account->password,
				'sec_pwd' => $account->sec_pwd,
				'units' => $account->target_value,
				'sell_type' => $account->target_value,
				'target_value' => $account->target_value,
			];
		}

		$res = $this->getConfigs();
		$res['accounts'] = $activeAccounts;

		return ['success'=>true, 'data'=>$res];
	}

	public function actionGetAllConfigs()
	{
		$activeAccounts = [];
		foreach(Accounts::findAll(['active'=>true]) as $account){
			$activeAccounts[] = [
				'username' => $account->username,
				'password' => $account->password,
				'sec_pwd' => $account->sec_pwd,
				'units' => $account->target_value,
				'sell_type' => $account->target_value,
				'target_value' => $account->target_value,
			];
		}

		$res = $this->getConfigs(true);
		$res['accounts'] = $activeAccounts;

		return ['success'=>true, 'data'=>$res];
	}

	public function actionSetConfigs()
	{
		$post = Yii::$app->request->post();

		foreach($post as $key=>$value){
			if($type = $this->getConfigType($key)){
				if( !$this->setConfig($key, $type, $value) ){
					return ['success'=>false, 'message'=>'Update Failed.'];
				}
			}else{
				return ['success'=>false, 'message'=>'Invalid Key'];
			}
		}
		return ['success'=>true, 'message'=>'Data Updated.'];

	}

	private function getConfigs($details=false, $jsonToArray=true)
	{
		if(!$model = SystemConfig::findOne('sys_user')){
			$model = new SystemConfig([
				'key' => 'sys_user',
				'type' => 'json',
				'value' => json_encode([
					'username' => '66635869',
					'password' => 'myles1101',
				]),
				'updated_at' => time(),
			]);
			$model->save();
		}

		if(!$model = SystemConfig::findOne('shares_price')){
			$model = new SystemConfig([
				'key' => 'shares_price',
				'type' => 'json',
				'updated_at' => time(),
			]);
			$model->save();
		}

		if(!$model = SystemConfig::findOne('processID')){
			$model = new SystemConfig([
				'key' => 'processID',
				'type' => 'integer',
				'updated_at' => time(),
			]);
			$model->save();
		}

		$res = [];

		foreach( SystemConfig::find()->all() as $conf ){
			switch($conf->type){
				case 'integer':
					$value = intval($conf->value);
					break;
				case 'float':
					$value = floatval($conf->value);
					break;
				case 'json':
					$value = ($jsonToArray) ? json_decode($conf->value, true) : $conf->value;
					break;
				default:
					$value = $conf->value;
					break;
			}

			if($details){
				$res[$conf->key] = [
					'value' => $value,
					'created_at' => $conf->created_at,
					'updated_at' => $conf->updated_at,
				];
			}else{
				$res[$conf->key] = $value;
			}
		}

		return $res;
	}

	private function getConfigType($key){
		
		if($model = SystemConfig::findOne($key) ){
			return $model->type;
		}else{
			return false;
		}
	}

	private function setConfig($key, $type, $value)
	{
		if($type=='json') $value = json_encode($value);
		if($model = SystemConfig::findOne(['key'=>$key])){
			$model->type = $type;
			$model->value = $value;
		}else{
			$model = new SystemConfig([
				'key' => $key,
				'type' => $type,
				'value' => (string)$value,
			]);
		}
		$model->updated_at = time();
		
		if(!$model->save()){
			print_r($model->getErrors());
			exit();
		}else{
			return true;
		}
	}
}
