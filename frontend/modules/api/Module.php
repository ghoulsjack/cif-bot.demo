<?php

namespace frontend\modules\api;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\api\controllers';

    public function init()
    {
        $this->on(self::EVENT_BEFORE_ACTION,function($event){
            \Yii::$app->request->enableCsrfValidation = false;
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            \Yii::$app->request->parsers = [ 'application/json' => 'yii\web\JsonParser'];
        });
        parent::init();
    }
}
