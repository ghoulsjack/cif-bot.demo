<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public function init()
    {
        $root = $this->getCss('css');
        $cdns = [
            'https://use.fontawesome.com/releases/v5.0.6/css/all.css',
            'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic'
        ];
        $this->css = array_merge_recursive($root, $cdns);
    }

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/adminlte.min.js',
        'js/demo.js',
        'js/angularjs/modules/angular.min.js',
        'js/angularjs/app.js',
        'js/angularjs/modules/angular-animate.min.js',
        'js/angularjs/modules/angular-touch.min.js',
        'js/angularjs/modules/ui-bootstrap-2.5.0.min.js',
        'js/angularjs/modules/ui-bootstrap-tpls-2.5.0.min.js',
        'js/angularjs/modules/api-check.min.js',
        'js/angularjs/modules/formly.min.js',
        'js/angularjs/modules/angular-formly-templates-bootstrap.js',

        // Configs
        'js/angularjs/configs/FormlyConfig.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    private function getCss($path)
    {
        $res = [];

        if(file_exists($path)){
            foreach(scandir($path) as $file){
                $fn = explode('.', $file);
                if($fn[count($fn) -1] == 'css'){
                    $res[] = $path.'/'.$file;
                }
            }
            
        }
        return $res;
    }

    private function getJs($path)
    {
        $res = [];
        if(file_exists($path)){
            foreach(scandir($path) as $file){
                $fn = explode('.', $file);
                if($fn[count($fn) -1] == 'js'){
                    $res[] = $path.'/'.$file;
                }
            }
        }
        return $res;
    }
}
