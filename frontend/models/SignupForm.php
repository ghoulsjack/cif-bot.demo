<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;
use common\models\CifAccounts;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $first_name;
    public $last_name;
    public $mobile;
    public $remark;

    public $account;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['account', 'required'],
            [['first_name', 'last_name', 'mobile', 'remark', 'account'], 'string'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->first_name = $this->first_name;
        $user->last_name = $this->last_name;
        $user->mobile = $this->mobile;
        $user->remark = $this->remark;
        $user->role = 'user';
        $user->generateAuthKey();

        if($user->save()){
            $account = new CifAccounts([
                'user_id' => $user->id,
                'username' => $this->account,
                'created_at' => time(),
                'updated_at' => time(),
            ]);
            $account->save();
            return $user;
        }
        return null;
    }
}
