import time
import sys

times = int(sys.argv[1])

if(times==0):
	i = 0
	while True:
		i=i+1
		with open("test.txt", "a") as myfile:
			myfile.write( str(i) + "\n" )
		time.sleep(1)
else:
	for i in range(int(times)):
		with open("test.txt", "a") as myfile:
			myfile.write( str(i+1) + "\n" )
		time.sleep(1)
	# end for
