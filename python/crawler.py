import requests
import shutil
import json
import base64
import sys
import time
# import urllib2
from bs4 import BeautifulSoup
from pathlib import Path


# 記錄開始時間
tic = time.clock()

# 取得command line 輸入片段
logID = sys.argv[1]
API_URL = sys.argv[2]

API_TOKEN = '8d92a727bdcddb01854c18be29e71e2b'

def getRunTime():
    toc = time.clock()

    return toc - tic
# end getRunTime()

def stop():
    processTime = getRunTime()

    writeLog('Process Time: ' + str(processTime))

    requests.post(API_URL + '/api/bot/stop', data={
        'API_TOKEN' : API_TOKEN,
    } )

    writeLog('Bot STOP')

# end stopTimer()

def writeLog(message, printLine=True):
    requests.post(API_URL + '/api/log/running', data={
        'API_TOKEN' : API_TOKEN,
        'logID': logID,
        'message' : message,
    } )

    if(printLine):
        print(message)
    # end if
# end writeLog()

def getSettings():
    res = requests.post(API_URL + '/api/bot/settings', data={
        'API_TOKEN' : API_TOKEN,
    } )
    return json.loads( res.text )
# end getSettings()

def updateSharesPrice(prices):
    res = requests.post(API_URL + '/api/bot/update-shares-price', data={
        'API_TOKEN' : API_TOKEN,
        'data':json.dumps(prices, False)
    } )
    res = json.loads( res.text )
    if (res['success']):
        writeLog('Shares Price Updated.')
    else:
        writeLog('Shares Price Update Failed.')
    # end if
    # return json.loads( res.text )
# end updateSharesPrice()

def setLastValue(username, value):
    data = {
        'API_TOKEN' : API_TOKEN,
        'username':username,
        'target_value':value
    }
    res = requests.post(API_URL + '/api/bot/update-last-value', data=data )
    writeLog('Update ' + username + ' Target Value to ' + str(value))
# end setLastValue()

# 取得百度API TOKEN
def getToken(API_KEY, SECRET_KEY):
    writeLog('Getting Baidu API TOKEN...')
    host = 'https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id='+API_KEY+'&client_secret='+SECRET_KEY
    baidu = requests.get(host, headers={'Content-Type': 'application/json; charset=UTF-8'})
    
    # f = open('baidu_token.json', 'w')
    # f.write(baidu.text)
    # f.close()
    token = json.loads(baidu.text)['access_token']
    writeLog('Baidu Token is: ' + token)
    return json.loads(baidu.text)
# end getToken()

# 使用百度API破解驗證碼
def generalBasic(encoded_string):
    writeLog('Calling Baidu OCR API...')
    # with open("captcha.png", "rb") as image_file:
    #     encoded_string = base64.b64encode(image_file.read())
    token = getToken('FV0KRy9Wmcu8NXl0AfaT2ry7', 'hXbez492YG9vstbaWypT2V0ZPI0c6fbb')
    res = requests.post('https://aip.baidubce.com/rest/2.0/ocr/v1/general_basic?access_token=' + token['access_token'], data={'image':encoded_string}, headers={'Content-type':'application/x-www-form-urlencoded'})
    data = json.loads(res.text)
    try:
        writeLog('done! result is: ' + data['words_result'][0]['words'])
        return data['words_result'][0]['words']
    except NameError:
        return False
# end generalBasic()

# 取得登入SESSION
def login(user, password, times=0):
    if(times >= 40):
        return False
    # endif
    writeLog('Logging in...')
    loginPage = 'https://member.cif-finance.com/login.php'
    targetPage = 'https://member.cif-finance.com/members/index.php'

    rs = requests.session()

    # 取得登入頁面資料
    res = rs.get(loginPage)

    # 擷取登入驗證碼
    soup = BeautifulSoup(res.text, 'html.parser')
    code2 = soup.find('input', {'name':'code2'})
    if code2 is None:
        writeLog('ERROR: Can\'t Find code2 Element.')
        writeLog('Try again after 30 sec.')

        time.sleep(30)
        return login(user, password, times+1)
    # endif
    code2 = code2['value']
    imgUrl = 'https://member.cif-finance.com/login.php?code=1&code2=' + code2
    cap = rs.get(imgUrl, stream=True, verify=False)

    # 呼叫百度API並取得驗證碼
    code = generalBasic(base64.b64encode(cap.content))
    if code == False:
        writeLog('ERROR: Can\'t Get Code')
        login(user, password, times+1)

    # 送出登入表單
    payload = {
        'do_login' : 1,
        'code2' : code2,
        'login_id' : user,
        'password' : password,
        'code': code
    }
    r = rs.post(loginPage, data=payload)

    if(r.url == targetPage):
        writeLog('Login Success!')
        times = 0
        return rs
    else:
        writeLog('Login Failed.. Try again! time: ' + str(times+1))
        return login(user, password, times+1)
        # endif
    # endif

# end login()
def logout(rs):
    rs.post('https://member.cif-finance.com/logout.php')
    writeLog('Bot Logout..');
# end logout()

# 使用SESSION擷取資料與判斷是否符合條件直到API回傳停止訊息
def checkData(accounts):
    global rs

    writeLog('Checking Data...')


    headers = requests.utils.default_headers()
    headers['User-Agent'] = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'

    res = rs.get('https://member.cif-finance.com/members/shares.php', headers=headers)

    # 取得表格資料
    soup = BeautifulSoup(res.text, 'html.parser')

    with open('page.html','wb') as f:
        f.write(res.content)
    # end with

    panels = soup.find_all('div', 'panel-default')

    if len(panels) < 5 :
    # if p is None:
        writeLog('Lost panels, re-login after 30 sec.')
        time.sleep(30)
        rs = login(sys_user['username'], sys_user['password'])
        return True
    # endif

    prices = []

    p = panels[4]

    for data in p.find_all('tr')[2:12]:
        td = data.find_all('td')

        tmp = {
            'id':td[0].string,
            'sell':td[1].string,
            'company':td[2].string,
            'market':td[3].string,
            'buy':td[4].string,
            'amount':td[5].string,
        }

        # 若sell不等於 "-" 則為有效資料
        if (tmp['sell'] != '-'):
            prices.append(tmp)
        # end if

    # end for

    # 更新資料庫資料
    updateSharesPrice(prices)

    writeLog('Checking All users below:');

    minTarget = 100;
    for account in accounts:
        writeLog(account['username'] + ': ' + str(account['target_value']));
        if ( account['target_value'] == None):
            account['target_value'] = prices[0]['sell']
            setLastValue(account['username'], prices[0]['sell'])
        # end if
        if (float(account['target_value']) < minTarget):
            minTarget = float(account['target_value'])
        # end if
    # end for

    writeLog('========== user list end ==========')
    writeLog('Min Target is ' + str(minTarget) + ', checking..')
    soldOut = True
    for price in prices:
        if( float(price['sell'])==minTarget ): #還沒賣完
            soldOut = False
            sleepTime = eval(price['id']) * 10
            writeLog('Not Sold, sleep ' + str(sleepTime) + ' sec.')
            # 依順序休息後重新檢查
            time.sleep(sleepTime)

            conf = getSettings()

            if(conf['success'] == True):
                conf = conf['data']
            # endif
            accounts = conf['accounts']
            # checkData(accounts)
            return True
        # end if
    # end for

    if(soldOut): #賣完了
        # 取價格最大值
        price = prices[-1]['sell']

        tradeAccounts = []

        for account in accounts:
            if(float(account['target_value']) == minTarget):
                tradeAccounts.append(account)
                setLastValue(account['username'], price)
            # end if 
        # end for

        writeLog('Sold out! Max Price is: ' + price)
        # 賣掉設定數量
        sellShare(tradeAccounts, price)
        # return False
        return True
    # end if
# end checkData()

# 依照價格變動拋售股票
def sellShare(accounts, price):
    writeLog('Selling shares of users below: ')
    for account in accounts:
        writeLog(account['username'] + ': ' + str(account['target_value']));

        try:
            tmp_rs = login(account['username'], account['password'])

            if(tmp_rs == False):
                writeLog('ERROR: Login Failed..')
            else:
                payload = {
                    '__req' : 1,
                    '_ttype' : 's', # 銷售
                    'sell_type' : account['sell_type'],
                    '_price' : price,
                    '_units' : account['units'],
                    'sec_pwd' : account['sec_pwd'],
                }

                res = tmp_rs.post('https://member.cif-finance.com/members/shares.php', data=payload)

                # 取得結果，若成功則回傳 (需測試20180329)
                soup = BeautifulSoup(res.text, 'html.parser')

                panels = soup.find_all('div', 'panel-default')
                error = panels[2].find_all('div', 'alert-danger')
                success = panels[2].find_all('div', 'alert-success')

                if(len(success)>0):
                    writeLog(account.username + ' Sold ' + str(price) + ', for ' + str(account.units))
                    return True
                elif(len(error)>0):
                    message = error[0].find_all('p', 'mb10')[0].contents[0]
                    writeLog('ERROR! msg: ' + message)
                    return False
                # end if
            # endif

        except Exception as e:
            writeLog(str(e))
        finally:
            logout(tmp_rs)
        # end try
    # end for
# end sellShare()

try:
    # START
    writeLog('Bot is running...')

    conf = getSettings()

    if(conf['success'] == True):
        conf = conf['data']
    # endif

    sys_user = conf['sys_user']
    accounts = conf['accounts']

    # 紀錄SESSION
    rs = login(sys_user['username'], sys_user['password'])

    if(rs == False):
        writeLog('ERROR: Login Failed..')
    else:
        checking = True
        while (checking):
            checking = checkData(accounts)
        # endwhile
    # endif

    logout(rs)

except Exception as e:
    writeLog(str(e))
finally:
    stop()
# end try


