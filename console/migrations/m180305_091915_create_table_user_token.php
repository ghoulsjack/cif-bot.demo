<?php

use yii\db\Migration;

/**
 * Class m180305_091915_create_table_user_token
 */
class m180305_091915_create_table_user_token extends Migration
{
    public function up()
    {
        $tableName = '{{%user_token}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema === null){
            $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

            $this->createTable($tableName, [
                'token' => $this->string(32)->unique(),
                'user_id' => $this->integer()->notNull(),
                'created_at' => $this->integer(),
                'expired_in' => $this->integer(),
            ], $tableOptions);
            
            $this->addPrimaryKey('user_token_pk', $tableName, ['token', ]);
            $this->addForeignKey('fk_user_token_user_id', $tableName, 'user_id', '{{%user}}', 'id', "CASCADE", "CASCADE");
        }

    }

    public function down()
    {
        $tableName = '{{%user_token}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema !== null) $this->dropTable($tableName);
    }
}
