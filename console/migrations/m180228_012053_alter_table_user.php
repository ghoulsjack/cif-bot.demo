<?php

use yii\db\Migration;

/**
 * Class m180228_012053_alter_table_user
 */
class m180228_012053_alter_table_user extends Migration
{
    public function up()
    {
        $tableName = '{{%user}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema !== null){
            $this->addColumn($tableName, 'role', $this->string(100)->after('id'));
        }
    }

    public function down()
    {
        $tableName = '{{%user}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema !== null){
            $this->dropColumn($tableName, 'role');
        }
    }
}
