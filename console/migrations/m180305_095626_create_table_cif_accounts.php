<?php

use yii\db\Migration;

/**
 * Class m180305_095626_create_table_cif_accounts
 */
class m180305_095626_create_table_cif_accounts extends Migration
{
    public function up()
    {
        $tableName = '{{%cif_accounts}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema === null){
            $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

            $this->createTable($tableName, [
                'user_id' => $this->integer()->notNull(),
                'username' => $this->string(255)->notNull(),
                'password' => $this->string(255)->notNull(),
                'sell_type' => $this->integer(5)->defaultValue(1),
                'units' => $this->integer(20)->notNull(),
                'sec_pwd' => $this->string(255)->notNull(),
                'active' => $this->boolean(),
                'started_at' => $this->integer(),
                'stoped_at' => $this->integer(),
                'last_value' => $this->string(255),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ], $tableOptions);
            
            $this->addPrimaryKey('cif_accounts_pk', $tableName, ['user_id', 'username']);
            $this->addForeignKey('fk_cif_accounts_user_id', $tableName, 'user_id', '{{%user}}', 'id', "CASCADE", "CASCADE");
        }

    }

    public function down()
    {
        $tableName = '{{%cif_accounts}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema !== null) $this->dropTable($tableName);
    }
}
