<?php

use yii\db\Migration;
use common\models\CifAccounts;

/**
 * Class m180329_070857_create_table_accounts
 */
class m180329_070857_create_table_accounts extends Migration
{
    public function up()
    {
        $tableName = '{{%accounts}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema === null){
            $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

            $this->createTable($tableName, [
                'id' => $this->primaryKey(),
                'username' => $this->string(255)->notNull(),
                'password' => $this->string(255)->notNull(),
                'remark' => $this->text(),
                'sell_type' => $this->integer(5)->defaultValue(1),
                'units' => $this->integer(20)->notNull(),
                'sec_pwd' => $this->string(255)->notNull(),
                'active' => $this->boolean(),
                'target_value' => $this->string(255),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ], $tableOptions);

            foreach( CifAccounts::find()->all() as $ca ){

                $this->insert($tableName,[
                    'username' => $ca->username,
                    'password' => $ca->password,
                    'sell_type' => $ca->sell_type,
                    'units' => $ca->units,
                    'sec_pwd' => $ca->sec_pwd,
                    'active' => $ca->active,
                    'target_value' => $ca->last_value,
                    'created_at' => time(),
                    'updated_at' => time(),
                ]);
            }

            $tableName = '{{%cif_accounts}}';

            $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

            if($tableSchema !== null) $this->dropTable($tableName);
        }

    }

    public function down()
    {
        $tableName = '{{%accounts}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema !== null) $this->dropTable($tableName);
    }
}
