<?php

use yii\db\Migration;

/**
 * Class m180329_064747_create_table_system_config
 */
class m180329_064747_create_table_system_config extends Migration
{
    public function up()
    {
        $tableName = '{{%system_config}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema === null){
            $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

            $this->createTable($tableName, [
                'key' => $this->string(255),
                'type' => $this->string(20),
                'value' => $this->text(10),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ], $tableOptions);

            $this->addPrimaryKey('system_config_pk', $tableName, ['key']);
        }
    }

    public function down()
    {
        $tableName = '{{%system_config}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema !== null) $this->dropTable($tableName);
    }
}
