<?php

use yii\db\Migration;

/**
 * Class m180307_040918_create_table_log_bot
 */
class m180307_040918_create_table_log_bot extends Migration
{
    public function up()
    {
        $tableName = '{{%log_bot}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema === null){
            $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

            $this->createTable($tableName, [
                'id' => $this->primaryKey(),
                'user_id' => $this->integer()->notNull(),
                'username' => $this->string(255)->notNull(),
                'status' => $this->integer()->defaultValue(0),
                'message' => $this->text(),
                'created_at' => $this->integer(),
            ], $tableOptions);

            $this->addForeignKey('fk_log_bot_user_id', $tableName, 'user_id', '{{%user}}', 'id', "CASCADE", "CASCADE");
        }

    }

    public function down()
    {
        $tableName = '{{%log_bot}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema !== null) $this->dropTable($tableName);
    }
}
