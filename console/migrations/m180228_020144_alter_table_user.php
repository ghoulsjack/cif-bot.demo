<?php

use yii\db\Migration;

/**
 * Class m180228_020144_alter_table_user
 */
class m180228_020144_alter_table_user extends Migration
{
    public function up()
    {
        $tableName = '{{%user}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema !== null){
            $this->addColumn($tableName, 'expired_at', $this->integer()->after('updated_at'));
            $this->addColumn($tableName, 'remark', $this->text()->after('role'));
            $this->addColumn($tableName, 'mobile', $this->string(10)->after('role'));
            $this->addColumn($tableName, 'realname', $this->string(20)->after('role'));
        }
    }

    public function down()
    {
        $tableName = '{{%user}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema !== null){
            $this->dropColumn($tableName, 'expired_at');
            $this->dropColumn($tableName, 'remark');
            $this->dropColumn($tableName, 'mobile');
            $this->dropColumn($tableName, 'realname');
        }
    }
}
