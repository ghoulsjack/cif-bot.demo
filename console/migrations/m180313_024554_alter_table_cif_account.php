<?php

use yii\db\Migration;

/**
 * Class m180313_024554_alter_table_cif_account
 */
class m180313_024554_alter_table_cif_account extends Migration
{
    public function up()
    {
        $tableName = '{{%cif_accounts}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema !== null){
            $this->addColumn($tableName, 'pid', $this->integer()->after('last_value'));
        }
    }

    public function down()
    {
        $tableName = '{{%cif_accounts}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema !== null){
            $this->dropColumn($tableName, 'pid');
        }
    }
}
