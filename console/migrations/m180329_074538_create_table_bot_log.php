<?php

use yii\db\Migration;

/**
 * Class m180329_074538_create_table_bot_log
 */
class m180329_074538_create_table_bot_log extends Migration
{
    public function up()
    {
        $tableName = '{{%bot_log}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema === null){
            $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

            $this->createTable($tableName, [
                'id' => $this->primaryKey(),
                'content' => $this->text(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ], $tableOptions);

        }

    }

    public function down()
    {
        $tableName = '{{%bot_log}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema !== null) $this->dropTable($tableName);
    }
}
