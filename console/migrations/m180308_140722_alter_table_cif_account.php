<?php

use yii\db\Migration;

/**
 * Class m180308_140722_alter_table_cif_account
 */
class m180308_140722_alter_table_cif_account extends Migration
{
    public function up()
    {
        $tableName = '{{%cif_accounts}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema !== null){
            $this->alterColumn($tableName, 'password', $this->string(255));
            $this->alterColumn($tableName, 'units', $this->integer(20));
            $this->alterColumn($tableName, 'sec_pwd', $this->string(255));
        }
    }

    public function down()
    {
        $tableName = '{{%cif_accounts}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema !== null){
            $this->alterColumn($tableName, 'password', $this->string(255))->notNull();
            $this->alterColumn($tableName, 'units', $this->integer(20))->notNull();
            $this->alterColumn($tableName, 'sec_pwd', $this->string(255))->notNull();
        }
    }
}
