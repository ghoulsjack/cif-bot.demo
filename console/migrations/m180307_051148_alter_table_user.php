<?php

use yii\db\Migration;

/**
 * Class m180307_051148_alter_table_user
 */
class m180307_051148_alter_table_user extends Migration
{
    public function up()
    {
        $tableName = '{{%user}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema !== null){
            $this->dropColumn($tableName, 'realname');
            $this->addColumn($tableName, 'first_name', $this->string()->after('role'));
            $this->addColumn($tableName, 'last_name', $this->string()->after('role'));
        }
    }

    public function down()
    {
        $tableName = '{{%user}}';

        $tableSchema = Yii::$app->db->schema->getTableSchema($tableName);

        if($tableSchema !== null){
            $this->dropColumn($tableName, 'first_name');
            $this->dropColumn($tableName, 'last_name');
            $this->addColumn($tableName, 'realname', $this->string(20)->after('role'));
        }
    }
}
