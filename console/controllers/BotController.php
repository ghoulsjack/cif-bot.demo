<?php

namespace console\controllers;

use Yii;
use common\models\User;

class BotController extends \yii\console\Controller
{
    public $message;
    
    public function options($actionID)
    {
        return ['message'];
    }
    
    public function optionAliases()
    {
        return [
        	'm' => 'message',
        ];
    }

    public function actionCaptcha()
    {
    	for($i=0;$i<999999;$i++){
    		$key = str_pad($i, 6, '0', STR_PAD_LEFT);
    		$data = file_get_contents('https://member.cif-finance.com/login.php?code=1&code2=' . $key);
    		file_put_contents(Yii::getAlias('@frontend/web/img/captcha/'.$key.'.png'), $data);
    	}
    	echo 'good through.';
    }
    
    public function actionIndex()
    {
        echo $this->message . "\n";
    }

    public function getUsers()
    {

    }

    public function crawler()
    {

    }
}
